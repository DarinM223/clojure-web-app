(ns compojure-tutorial.core
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.util.response :refer [resource-response response]]
            [ring.middleware.json :as middleware]
            [ring.middleware.defaults :refer [wrap-defaults api-defaults]]
            [compojure-tutorial.models :as models])
  (:use [hiccup core page]
        [korma.db]))

(defn user-routes []
  (routes
    (GET "/:id" [id] (response (models/get-user id)))
    (POST "/" req
      (let [name (get (:params req) :name)
            email (get (:params req) :email)]
        (models/insert-user name email)))))

(defn api-routes []
  (routes
    (GET "/" [] (response {:hello "Hello"}))
    (context "/users" []
      (user-routes))))

(defroutes app-routes
  (GET "/" []
    (html5
      [:head
        [:title "Hello world"]]
      [:body
        [:h1 "Hi world"]]))
  ; Sub routes
  (context "/api" []
     (api-routes))
  (route/not-found "<h1>Page not found</h1>"))

(def app
  ; Add middleware to app routes
  (-> app-routes
      (middleware/wrap-json-body)
      (middleware/wrap-json-response)
      (wrap-defaults api-defaults)))
