(ns compojure-tutorial.models
  (:use [korma.db]))

;; (defdb db (postgres {:db "mydb"
;;                     :user "user"
;;                     :password "dbpass"}))
;; Use this when connecting to postgres

(defdb db (sqlite3 {:db "test.db"}))

(use 'korma.core)

(defentity users
  (pk :id))

(defn get-user [id]
  (select users
    (where {:id [= id]})))

(defn insert-user [name email]
  (insert users
    (values {:name name
             :email email})))
