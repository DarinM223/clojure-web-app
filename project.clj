(defproject compojure-tutorial "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [compojure "1.4.0"]
                 [hiccup "1.0.5"]
                 [ring/ring-core "1.4.0"]
                 [ring/ring-defaults "0.1.5"]
                 [ring/ring-json "0.4.0"]
                 [korma "0.4.2"]
                 [org.clojure/java.jdbc "0.3.7"]
                 [sqlitejdbc "0.5.6"]
                 [ragtime "0.5.1"]]
  :plugins [[lein-ring "0.8.11"]]
  :ring {:handler compojure-tutorial.core/app}
  :aliases {"migrate" ["run" "-m" "compojure-tutorial.db/migrate"]
            "rollback" ["run" "-m" "compojure-tutorial.db/rollback"]})
