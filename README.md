# compojure-tutorial

A tutorial that shows how to use compojure to make routes, how to use ragtime to handle database migrations, how to use korma to access database tables, how to use hiccup to render html in clojure, and how to use ring to make a auto-reloading server.

## Usage

To run the server:

    lein ring server

